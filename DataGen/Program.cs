﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DataGen
{
    class Program
    {
        class Box
        {
            int t;
            int x;
            int y;
            int z;
            int mass;
            int price;

            public Box(int t, int x, int y, int z)
            {
                this.t = t;
                this.x = x;
                this.y = y;
                this.z = z;
            }

            public Box(Box b)
            {
                this.t = b.t;
                this.x = b.x;
                this.y = b.y;
                this.z = b.z;
            }

            public override string ToString()
            {
                return t.ToString() + ',' + x.ToString() + ',' + y.ToString() + ',' + z.ToString();
            }
        }

        static void Main(string[] args)
        {
            //Пресеты картонных коробок из ГОСТ 12301-81

            //Малые картонные коробки
            List<Box> SmallBoxes = new List<Box>();
            SmallBoxes.Add(new Box(1, 200, 120, 70));
            SmallBoxes.Add(new Box(2, 215, 160, 95));
            SmallBoxes.Add(new Box(3, 250, 180, 125));

            SmallBoxes.Add(new Box(4, 270, 200, 126));
            SmallBoxes.Add(new Box(5, 305, 215, 130));
            SmallBoxes.Add(new Box(6, 340, 120, 150));

            SmallBoxes.Add(new Box(7, 350, 228, 195));
            SmallBoxes.Add(new Box(8, 355, 240, 200));
            SmallBoxes.Add(new Box(9, 365, 250, 250));

            SmallBoxes.Add(new Box(10, 380, 285, 287));
            SmallBoxes.Add(new Box(11, 500, 325, 300));
            SmallBoxes.Add(new Box(12, 650, 350, 350));

            //Крупногабаритные картонные коробки
            SmallBoxes.Add(new Box(13, 790, 390, 375));
            SmallBoxes.Add(new Box(14, 1020, 600, 430));
            SmallBoxes.Add(new Box(15, 1340, 630, 470));

            Random random = new Random();
            List<Box> ResultBoxes = new List<Box>();
            int m = 7200;
            for (int i = 0; i < m; i++)
            {
                double v = random.Next(0, 24);
                if (v < 1)
                {
                    ResultBoxes.Add(new Box(SmallBoxes[random.Next(12, 15)]));
                }
                else if (v < 3)
                {
                    ResultBoxes.Add(new Box(SmallBoxes[random.Next(9, 12)]));
                }
                else if (v < 6)
                {
                    ResultBoxes.Add(new Box(SmallBoxes[random.Next(6, 9)]));
                }
                else if (v < 12)
                {
                    ResultBoxes.Add(new Box(SmallBoxes[random.Next(3, 6)]));
                }
                else
                {
                    ResultBoxes.Add(new Box(SmallBoxes[random.Next(0, 3)]));
                }
            }

            string Result = "";
            int t = 0;
            foreach (Box b in ResultBoxes)
            {
                Result += b.ToString() + Environment.NewLine;
            }
            File.WriteAllText("../../../../data.csv", Result);

            Console.WriteLine("Hello World!");
        }
    }
}
