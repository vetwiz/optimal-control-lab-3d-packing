﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Solver
{
    public class Box
    {
        //Свойства коробки вне машины
        public int id;
        public int t;//Тип
        public int x;//Ширина
        public int y;//Длина
        public int z;//Высота
        public double v;//Объем

        //Свойства коробки в машине
        public Car car;//Машина
        public Box pbox;//Предыдущая коробка, null если это первая коробка в машине
        public Box xbox;//Коробка сбоку, null если место свободно
        public Box ybox;//Коробка спереди, null если место свободно
        public Box zbox;//Коробка сверху, null если место свободно
        public int xpos = -1;//Позиция в машине x
        public int ypos = -1;//Позиция в машине y 
        public int zpos = -1;//Позиция в машине z
        //Минимальное расстояние точки прилегания коробки или минимальное из таких значений от прилегающей коробки
        public double xmindist = -1;//Дистанция стороны X от 0, double.MaxValue - нет места под еще одну коробку
        public double ymindist = -1;//Дистанция стороны Y от 0, double.MaxValue - нет места под еще одну коробку
        public double zmindist = -1;//Дистанция стороны Z от 0, double.MaxValue - нет места под еще одну коробку

        public Box(string BoxLine)
        {
            var b = BoxLine.Split(',');
            t = Convert.ToInt32(b[0]);
            x = Convert.ToInt32(b[1]);
            y = Convert.ToInt32(b[2]);
            z = Convert.ToInt32(b[3]);
            v = x * y * z;
        }

        //Проверка возможности поставить коробку не пересекая другие коробки и стены машины
        //TODO: добавить возможность ставить коробку только если все 4 точки лежат поверхности других коробок
        public bool CheckSide(int xmax, int ymax, int zmax, int xmin, int ymin, int zmin, int boxid)
        {
            if ( xpos > xmax || ypos > ymax || zpos > zmax)
            {
                return true;
            }
            else if (zpos + z > zmin && xpos + x > xmin && ypos + y > ymin)
            {
                return false;
            }
            else
            {
                bool result = true;
                if (pbox != null)
                {
                    if (pbox.id != boxid)
                    {
                        result = result && pbox.CheckSide(xmax, ymax, zmax, xmin, ymin, zmin, id);
                    }
                }
                if (xbox != null)
                {
                    if(xbox.id != boxid)
                    {
                        result = result && xbox.CheckSide(xmax, ymax, zmax, xmin, ymin, zmin, id);
                    }
                }
                if (ybox != null)
                {
                    if (ybox.id != boxid)
                    {
                        result = result && ybox.CheckSide(xmax, ymax, zmax, xmin, ymin, zmin, id);
                    }
                }
                if (zbox != null)
                {
                    if (zbox.id != boxid)
                    {
                        result = result && zbox.CheckSide(xmax, ymax, zmax, xmin, ymin, zmin, id);
                    }
                }
                return result;
            }
            
        }

        //Приставить к этой коробке еще одну коробку из списка коробок
        public bool AppendBox(int alg, BoxList boxes)
        {
            if(xmindist <= ymindist && xmindist <= zmindist)
            {//Присоединяем со стороны X
                if(xbox!=null)
                {
                    bool result = xbox.AppendBox(alg, boxes);
                    RecalcDist();
                    return result;
                }
                else
                {
                    int t = 1;
                    if(alg == 1)
                    {
                        t = 15;
                    }
                    else
                    {
                        t = 1;
                    }
                    while (t < 16 && t > 0)//Пробуем поставить все типы коробок, начиная с маленьких
                    {
                        try
                        {
                            Box b = Program.GetFreeBox(t, boxes);//Тут может вылететь эксепшен, если коробок типа t нет
                            if ((b.y + ypos < car.y) && (b.x + xpos + x < car.x) && (b.z + zpos < car.z) && CheckSide(xpos + x + b.x, ypos + b.y, zpos + b.z, xpos + x, ypos, zpos, id))
                            {
                                boxes.used += 1;
                                xbox = b;
                                b.pbox = this;
                                b.car = car;
                                b.xpos = xpos + x;
                                b.ypos = ypos;
                                b.zpos = zpos;
                                b.RecalcDist();
                                RecalcDist();
                                //Console.WriteLine("bt" + b.t + "bxp" + b.xpos + "byp" + b.ypos + "bzp" + b.zpos + "pid" + id + "cid" + b.id + "bz" + b.z + "z" + z + "by" + b.y + "y" + y + "bx" + b.x + "x" + x + "xp" + xpos + "yp" + ypos + "zp" + zpos);
                                return true;//Добавили коробку, нужно пересчитать дистанции всех коробок до этой
                            }
                        }
                        catch { }
                        if(alg==1)
                        {
                            t--;
                        }
                        else
                        {
                            t++;
                        }
                    }
                    xmindist = double.MaxValue;
                    return false;
                }
            }
            else if(ymindist <= xmindist && ymindist <= zmindist)
            {
                if (ybox != null)
                {
                    bool result = ybox.AppendBox(alg, boxes);
                    RecalcDist();
                    return result;
                }
                else
                {
                    int t = 1;
                    if (alg == 1)
                    {
                        t = 15;
                    }
                    else
                    {
                        t = 1;
                    }
                    while (t < 16 && t > 0)//Пробуем поставить все типы коробок, начиная с маленьких
                    {
                        try
                        {
                            Box b = Program.GetFreeBox(t, boxes);//Тут может вылететь эксепшен, если коробок типа t нет
                            if ((b.y + ypos + y < car.y) && (b.x + xpos < car.x) && (b.z + zpos < car.z) && CheckSide(xpos + b.x, ypos + y + b.y, zpos + b.z, xpos, ypos + y, zpos, id))
                            {
                                boxes.used += 1;
                                ybox = b;
                                b.pbox = this;
                                b.car = car;
                                b.xpos = xpos;
                                b.ypos = ypos + y;
                                b.zpos = zpos;
                                b.RecalcDist();
                                RecalcDist();
                                //Console.WriteLine("bt" + b.t + "bxp" + b.xpos + "byp" + b.ypos + "bzp" + b.zpos + "pid" + id + "cid" + b.id + "bz" + b.z + "z" + z + "by" + b.y + "y" + y + "bx" + b.x + "x" + x + "xp" + xpos + "yp" + ypos + "zp" + zpos);
                                return true;//Добавили коробку, нужно пересчитать дистанции всех коробок до этой
                            }
                        }
                        catch { }
                        if (alg == 1)
                        {
                            t--;
                        }
                        else
                        {
                            t++;
                        }
                    }
                    ymindist = double.MaxValue;
                    return false;
                }
            }
            else if(zmindist <= xmindist && zmindist <= ymindist)
            {
                if (zbox != null)
                {
                    bool result = zbox.AppendBox(alg, boxes);
                    RecalcDist();
                    return result;
                }
                else
                {
                    int t = 1;
                    if (alg == 1)
                    {
                        t = 15;
                    }
                    else
                    {
                        t = 1;
                    }
                    while (t < 16 && t > 0)//Пробуем поставить все типы коробок, начиная с маленьких
                    {
                        try
                        {
                            Box b = Program.GetFreeBox(t, boxes);//Тут может вылететь эксепшен, если коробок типа t нет
                            if ((b.y + ypos < car.y) && (b.x + xpos < car.x) && (b.z + zpos + z < car.z) && CheckSide(xpos + b.x, ypos + b.y, zpos + z + b.z, xpos, ypos, zpos + z, id))
                            {
                                boxes.used += 1;
                                zbox = b;
                                b.pbox = this;
                                b.car = car;
                                b.xpos = xpos;
                                b.ypos = ypos;
                                b.zpos = zpos + z;
                                b.RecalcDist();
                                RecalcDist();
                                //Console.WriteLine("bt" + b.t + "bxp" + b.xpos + "byp" + b.ypos + "bzp" + b.zpos + "pid" + id + "cid" + b.id +"bz" + b.z + "z" + z + "by" + b.y + "y" + y + "bx" + b.x +"x" + x + "xp" + xpos + "yp" + ypos + "zp" + zpos);
                                return true;//Добавили коробку, нужно пересчитать дистанции всех коробок до этой
                            }
                        }
                        catch { }
                        if (alg == 1)
                        {
                            t--;
                        }
                        else
                        {
                            t++;
                        }
                    }
                    zmindist = double.MaxValue;
                    return false;//Сюда не поместить никакую коробку, нужно пересчитать дистанции предыдущих коробок 
                }
            }
            else
            {
                throw new Exception("AAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            }
        }

        public void RecalcDist()
        {
            if (xbox == null)
            {
                if (xmindist != double.MaxValue)
                {
                    xmindist = Math.Sqrt((xpos + x) * (xpos + x) + ypos * ypos + zpos * zpos);
                }
            }
            else
            {
                if(xmindist != double.MaxValue)
                {
                    xmindist = Math.Min(xbox.xmindist, Math.Min(xbox.ymindist, xbox.zmindist));
                }
            }

            if (ybox == null)
            {
                if (ymindist != double.MaxValue)
                {
                    ymindist = Math.Sqrt(xpos * xpos + (ypos + y) * (ypos + y) + zpos * zpos);
                }
            }
            else
            {
                if (ymindist != double.MaxValue)
                {
                    ymindist = Math.Min(ybox.xmindist, Math.Min(ybox.ymindist, ybox.zmindist));
                }
            }

            if (zbox == null)
            {
                if (zmindist != double.MaxValue)
                {
                    zmindist = Math.Sqrt(xpos * xpos + ypos * ypos + (zpos + z) * (zpos + z));
                }
            }
            else
            {
                if (zmindist != double.MaxValue)
                {
                    zmindist = Math.Min(zbox.xmindist, Math.Min(zbox.ymindist, zbox.zmindist));
                }
            }
        }

        public bool HaveSpace()
        {
            return (Math.Min(xmindist, Math.Min(ymindist, zmindist)) != double.MaxValue);
        }
    }

    public class Car
    {
        public int x;
        public int y;
        public int z;
        public int v;

        public Box pbox;//Первая коробка в машине

        public Car(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            v = x * y * z;
        }
    }

    public class BoxList : List<Box>
    {
        public int used;
    }

    public static class Program
    {
        public static Box GetFreeBox(int t, BoxList boxes)
        {
            for (int i = 0; i < boxes.Count; i++)
            {
                if (boxes[i].car == null && boxes[i].t == t)
                {
                    return boxes[i];
                }
            }
            throw new Exception("No boxes left");
        }

        public static Box GetFreeSmallBox(BoxList boxes)
        {
            Box b = new Box("1,1,1,1");
            for (int type = 1; type < 16; type++)
            {
                try
                {
                    b = GetFreeBox(type, boxes);
                    break;
                }
                catch { }
            }
            if (b.x != 1)
                return b;
            else
                throw new Exception("No boxes left");
        }

        public static Box GetFreeBigBox(BoxList boxes)
        {
            Box b = new Box("1,1,1,1");
            for (int type = 15; type > 0; type--)
            {
                try
                {
                    b = GetFreeBox(type, boxes);
                    break;
                }
                catch { }
            }
            if (b.x != 1)
                return b;
            else
                throw new Exception("No boxes left");
        }


        public static bool HaveFreeBox(BoxList boxes)
        {
            for(int i = 0; i < boxes.Count; i++)
            {
                if(boxes[i].pbox == null)
                {
                    return true;
                }
            }
            return false;
        }

        public static double SummBoxesV(BoxList boxes)
        {
            double v = 0;
            for(int i = 0; i < boxes.Count; i++)
            {
                v += boxes[i].v;
            }
            return v;
        }

        public static void Main(string[] args)
        {
            BoxList boxes = new BoxList();
            List<Car> cars = new List<Car>();
            string[] data = File.ReadAllLines("../../../../data.csv");
            foreach (string v in data)
            {
                boxes.Add(new Box(v));
            }
            for (int i = 0; i < boxes.Count; i++)
            {
                boxes[i].id = i;
            }

            int cid = 0;
            while (boxes.used < boxes.Count)
            {
                cars.Add(new Car(1978, 3056, 1565));
                boxes.used += 1;
                cars[cid].pbox = GetFreeBigBox(boxes);
                cars[cid].pbox.car = cars[cid];
                cars[cid].pbox.xpos = 0;
                cars[cid].pbox.ypos = 0;
                cars[cid].pbox.zpos = 0;
                cars[cid].pbox.RecalcDist();

                while (boxes.used < boxes.Count && cars[cid].pbox.HaveSpace())
                {
                    cars[cid].pbox.AppendBox(1, boxes);
                    //string s = Console.ReadLine();
                }
                cid++;
            }

            double carv = 9460061920;
            double summcarsv = cars.Count * carv;
            double summboxesv = SummBoxesV(boxes);
            double eff = summboxesv / summcarsv;

            Console.WriteLine("Boxes: " + boxes.used);
            Console.WriteLine("Cars: " + cars.Count);
            Console.WriteLine("All cars V summ: " + summcarsv);
            Console.WriteLine("All boxes V summ: " + summboxesv);
            Console.WriteLine("Vboxes/Vcars: " + eff);
            Main2(args);
        }

        public static void Main2(string[] args)
        {
            BoxList boxes = new BoxList();
            List<Car> cars = new List<Car>();
            string[] data = File.ReadAllLines("../../../../data.csv");
            foreach (string v in data)
            {
                boxes.Add(new Box(v));
            }
            for (int i = 0; i < boxes.Count; i++)
            {
                boxes[i].id = i;
            }

            int cid = 0;
            while (boxes.used < boxes.Count)
            {
                cars.Add(new Car(1978, 3056, 1565));
                boxes.used += 1;
                cars[cid].pbox = GetFreeSmallBox(boxes);
                cars[cid].pbox.car = cars[cid];
                cars[cid].pbox.xpos = 0;
                cars[cid].pbox.ypos = 0;
                cars[cid].pbox.zpos = 0;
                cars[cid].pbox.RecalcDist();

                while (boxes.used < boxes.Count && cars[cid].pbox.HaveSpace())
                {
                    cars[cid].pbox.AppendBox(0, boxes);
                    //string s = Console.ReadLine();
                }
                cid++;
            }

            double carv = 9460061920;
            double summcarsv = cars.Count * carv;
            double summboxesv = SummBoxesV(boxes);
            double eff = summboxesv / summcarsv;

            Console.WriteLine("Boxes: " + boxes.used);
            Console.WriteLine("Cars: " + cars.Count);
            Console.WriteLine("All cars V summ: " + summcarsv);
            Console.WriteLine("All boxes V summ: " + summboxesv);
            Console.WriteLine("Vboxes/Vcars: " + eff);
            string ss = Console.ReadLine();
            Console.WriteLine(ss);
        }
    }
}
